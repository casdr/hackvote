window.socket.emit('type', 'team');

var myTeam = null;

$('#waiting,#voting,#thanks').hide();

$('#loginTeam').click(function () {
  $('#login').hide();
  myTeam = $('#teamName').val();
  window.socket.emit('loginTeam', $('#teamName').val());
  $('#waiting').show();
});

window.socket.on('startVoting', function (teams) {
  $('#waiting').hide();
  $('#voting').show();
  $('#votinglist').html('');
  var total = Object.keys(teams).length;
  for (var teamName in teams) {
    if (teamName == myTeam) {
      continue;
    }
    var tab = $('#votinglist');
    var current = 1;
    var line = '<tr>';
    line += '<td>' + teamName + '</td>';
    while (current < total) {
      line += '<td><input type="radio" name="score_' + current + '" value="' + teamName + '" data-team-name="' + teamName + '" data-team="' + teamName.replace(' ', '_') + '" class="voteselect" required="required">' + current + '</td>';
      current++;
    }
    tab.append(line);
  }
});

$(document).on('click', '.voteselect', function () {
  var team = $(this).attr('data-team');
  $('[data-team=' + team + ']').prop('checked', false);
  $(this).prop('checked', true);
});

$(document).on('click', '#voteSubmit', function () {
  var votes = {};
  $('.voteselect:checked').each(function () {
    console.log(this);
    var teamName = $(this).val();
    var teamVotes = parseInt($(this).attr('name').replace('score_', ''));
    votes[teamName] = teamVotes;
  });
  console.log(votes);
  $('#voting').hide();
  $('#thanks').show();
  window.socket.emit('insertVotes', votes);
});