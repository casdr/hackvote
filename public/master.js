window.socket.emit('type', 'master');

$('#voting').hide();

window.socket.on('teamList', function(teams) {
  $('#teamlist').html('');
  for (var teamName in teams) {
    $('#teamlist').append($('<li>').text(teamName));
  }
});

window.socket.on('votingList', function(teams) {
  $('#start').hide();
  $('#voting').show();
  $('#votinglist > tbody').html('');
  for (var teamName in teams) {
    var votes = teams[teamName];
    $('#votinglist > tbody').append($('<tr><td>' + teamName + '</td><td>' + votes + '</td></tr>'));
  }
});

window.socket.on('startVoting', function () {
  $('#start').hide();
  $('#voting').show();
});

$('#startVoting').click(function () {
  window.socket.emit('startVoting');
});