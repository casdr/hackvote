express = require 'express'
app = express()
http = require('http').Server(app)
io = require('socket.io')(http)

app.use express.static 'public'

voting = false
teams = {}

io.on 'connection', (socket) ->
  socket.on 'type', (type) ->
    socket.host_type = type
    if socket.host_type == 'master'
      if voting
        socket.emit 'startVoting', teams
      else
        socket.emit 'teamList', teams

  socket.on 'loginTeam', (teamName) ->
    teams[teamName] = 0
    io.emit 'teamList', teams

  socket.on 'startVoting', ->
    voting = true
    io.emit 'startVoting', teams

  socket.on 'insertVotes', (votes) ->
    for teamName,number of votes
      console.log teamName, number
      teams[teamName] = teams[teamName] + number
    console.log teams
    io.emit 'votingList', teams

http.listen 1447, () ->
  console.log "Ready"